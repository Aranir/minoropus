using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public enum IconsEnum
{
		Move,
		Attack,
		Entity,
		Hero,
		Enemy,
		Action,
		Pussy,
		Bow,
		EqualTo,
		MoreThan,
		LessThan,
		One,
		Two,
		Three,
		Four,
		Range
}
;



public class GameEngine : MonoBehaviour
{
		public Sprite attachSprite;
		public Sprite moveSprite;
		public  Sprite weaponSprite;
		public Sprite heroSprite;
		public Sprite enemySprite;
		public Sprite pussySprite;
		public Sprite bowSprite;
		public Sprite equalToSprite;
		public Sprite moreThanSprite;
		public Sprite lessThanSprite;
		public Sprite oneSprite;
		public Sprite twoSprite;
		public Sprite threeSprite;
		public Sprite fourSprite;
		public Sprite rangeSprite;


		public List<TurnAction> heroActions;
		protected List<TurnAction> enemyActions;
		protected bool heroeDone;
		protected bool enemyDone;
		protected int actionIndex;
		protected int enemyActionIndex;
		public GameObject iconPrefab;
		public GameObject slotPrefab;
		public Canvas slotCanvas;
		public Canvas iconCanvas;
		public Canvas conditionCanvas;
		public static int slotSize = 40;
		public GameState gameState;
		public static int space = 10;
		public List<Slot> actionSlots;
		public List<Slot> condSlots;
		public bool showSlots;
		static public CanvasGroup toDissappearCanvas = null;
		public Button Action;
		public Button Condition;

		public bool showSlots2 = true;

	public int round =-1;
	public Text t1 ;
	public int heroHealth;
	public Text t2;
	public int enemyHealth;
	public Text t7;
	public Text t8;
	public Text t9;

		// Use this for initialization
		protected void Start ()
		{
		round = -1;
				actionSlots = new List<Slot> ();
				this.showSlots = true;
				this.actionIndex = 0;
				this.enemyActionIndex = 0;
				heroActions = new List<TurnAction> ();
				enemyActions = new List<TurnAction> ();
				int numberOfSlots = 10;
				for (int i = 0; i < numberOfSlots; i++) {

						if(i == 0 ){
				addThreeIcon(new Vector3 (GameEngine.slotSize + (GameEngine.slotSize + space) * i, GameEngine.slotSize, 0));
							continue;
						}

						GameObject slot = (GameObject)Instantiate (slotPrefab);
						Slot script = slot.GetComponent<Slot> ();
						script.gameEngine = this;
						script.gameState = this.gameState;
						slot.transform.position = new Vector3 (GameEngine.slotSize + (GameEngine.slotSize + space) * i, GameEngine.slotSize, 0);
						slot.transform.parent = slotCanvas.transform;
						script.height = GameEngine.slotSize;
						actionSlots.Add (script);
				}

				for (int i = 0; i < numberOfSlots; i++) {
			if(i == 0 ){
				addTwoIcon(new Vector3 (GameEngine.slotSize + (GameEngine.slotSize + space) * i, GameEngine.slotSize * 2 + 20, 0));
				continue;
			}


						GameObject slot = (GameObject)Instantiate (slotPrefab);
						Slot script = slot.GetComponent<Slot> ();
						script.gameEngine = this;
						script.gameState = this.gameState;
						slot.transform.position = new Vector3 (GameEngine.slotSize + (GameEngine.slotSize + space) * i, GameEngine.slotSize * 2 + 20, 0);
						slot.transform.parent = slotCanvas.transform;
						actionSlots.Add (script);

				}

				for (int i = 0; i < numberOfSlots; i++) {
			if(i == 0 ){
				addOnenIcon(new Vector3 (GameEngine.slotSize + (GameEngine.slotSize + space) * i, GameEngine.slotSize * 3 + 40, 0));
				continue;
			}
						GameObject slot = (GameObject)Instantiate (slotPrefab);
						Slot script = slot.GetComponent<Slot> ();
						script.gameEngine = this;
						script.gameState = this.gameState;
						slot.transform.position = new Vector3 (GameEngine.slotSize + (GameEngine.slotSize + space) * i, GameEngine.slotSize * 3 + 40, 0);
						slot.transform.parent = slotCanvas.transform;
						actionSlots.Add (script);

				}

		
				for (int i = 0; i < 3; i++) {
						GameObject slotp = (GameObject)Instantiate (slotPrefab);
						Slot scriptp = slotp.GetComponent<Slot> ();
						scriptp.gameEngine = this;
						scriptp.gameState = this.gameState;
						slotp.transform.position = new Vector3 (GameEngine.slotSize + (GameEngine.slotSize + space) * i + 100, GameEngine.slotSize * 2 + 40, 0);
						slotp.transform.parent = conditionCanvas.transform;
						scriptp.height = GameEngine.slotSize;
						condSlots.Add (scriptp);
				}
		initScores ();
	}

	public void initScores(){
		GameObject o = GameObject.Find ("/topScreen/healthH");
		t1 = o.GetComponent<Text> ();
		heroHealth = this.gameState.hero.health;
		t1.text = ""+ heroHealth;
		
		o = GameObject.Find ("/topScreen/healthE");
		t2 = o.GetComponent<Text> ();
		enemyHealth = this.gameState.enemy.health;
		t2.text =""+ enemyHealth;
		
		o = GameObject.Find ("/topScreen/attackH");
		Text t3 = o.GetComponent<Text> ();
		t3.text = "" + this.gameState.hero.weapon.damage;
		
		o = GameObject.Find ("/topScreen/attackE");
		Text t4 = o.GetComponent<Text> ();
		Debug.Log (this.gameState.enemy.weapon);
		if (this.gameState.enemy.weapon != null)
			t4.text = "" + this.gameState.enemy.weapon.damage;
		else
			t4.text = "0";
		
		o = GameObject.Find ("/topScreen/rangeH");
		Text t5 = o.GetComponent<Text> ();
		t5.text = "" + this.gameState.hero.weapon.range;
		
		o = GameObject.Find ("/topScreen/rangeE");
		Text t6 = o.GetComponent<Text> ();
		
		if (this.gameState.enemy.weapon != null)
			t6.text = "" + this.gameState.enemy.weapon.range;
		else
			t6.text = "0";
		
		o = GameObject.Find ("/topScreen/actionpH");
		t7 = o.GetComponent<Text> ();
		t7.text =""+ this.gameState.hero.actionPoints;
		
		o = GameObject.Find ("/topScreen/actionpE");
		t8 = o.GetComponent<Text> ();
		t8.text =""+ this.gameState.enemy.actionPoints;
		
		o = GameObject.Find ("/topScreen/round");
		t9= o.GetComponent<Text> ();
		t9.text = "Round 0";

		}
		


		public void addAction (TurnAction action)
		{
				this.heroActions.Add (action);
		}

		public void setActions (List<TurnAction> actions)
		{
				this.heroActions = actions;
		}

		
	
		// Update is called once per frame
		public void Update ()
		{

				if(gameState.hero.health!=heroHealth){
			heroHealth = gameState.hero.health;
			t1.text =""+ heroHealth;
				}

		if(gameState.enemy.health!=enemyHealth){
			Debug.Log(gameState.enemy);
			enemyHealth = gameState.enemy.health;
			t2.text =""+enemyHealth ;
		}

		t7.text = ""+ gameState.hero.actionPoints;
		t8.text =""+  gameState.enemy.actionPoints;

				CanvasGroup group = slotCanvas.GetComponent<CanvasGroup> ();
				if (!this.showSlots && group.alpha > 0.1f) {
						group.alpha *= 0.90f;
				}
				if (!this.showSlots && group.alpha < 0.1f) {
						group.alpha = 0.0f;
						this.showSlots = true;
				}
				


				CanvasGroup group1 = conditionCanvas.GetComponent<CanvasGroup> ();
				if (!this.showSlots2 && group1.alpha > 0.1f) {
						group1.alpha *= 0.90f;
				}
				if (!this.showSlots && group.alpha < 0.1f) {
						group.alpha = 0.0f;
						this.showSlots = true;
				}

	
		}

		public void OnPlay ()
		{
				Debug.Log ("palying + sluts " + actionSlots.Count);
				foreach (Slot slot in actionSlots) {
						if (slot.action != null) {
								Debug.Log ("add attacking" + slot.action);

								heroActions.Add (slot.action);
						}
				}
				StartTurn ();
		}

		public void OnActionPress ()
		{
				GameObject canvas = GameObject.Find ("/SlotCanvas");
				Canvas canv = canvas.GetComponent<Canvas> ();
				CanvasGroup cgroup = canv.GetComponent<CanvasGroup> ();
				cgroup.interactable = true;
				cgroup.blocksRaycasts = true;
				slotCanvas.GetComponent<CanvasGroup> ().alpha = 1.0f;
		
				GameObject canvas1 = GameObject.Find ("/ConditionCanvas");
				Canvas canv1 = canvas1.GetComponent<Canvas> ();
				CanvasGroup cgroup1 = canv1.GetComponent<CanvasGroup> ();
				cgroup1.interactable = false;
				cgroup1.blocksRaycasts = false;
				conditionCanvas.GetComponent<CanvasGroup> ().alpha = 0.0f;

		}


		public void OnConditionPress ()
		{
		
				GameObject canvas = GameObject.Find ("/SlotCanvas");
				Canvas canv = canvas.GetComponent<Canvas> ();
				CanvasGroup cgroup = canv.GetComponent<CanvasGroup> ();
				cgroup.interactable = false;
				cgroup.blocksRaycasts = false;
				slotCanvas.GetComponent<CanvasGroup> ().alpha = 0.0f;

				GameObject canvas1 = GameObject.Find ("/ConditionCanvas");
				Canvas canv1 = canvas1.GetComponent<Canvas> ();
				CanvasGroup cgroup1 = canv1.GetComponent<CanvasGroup> ();
				cgroup1.interactable = true;
				cgroup1.blocksRaycasts = true;
				conditionCanvas.GetComponent<CanvasGroup> ().alpha = 1.0f;
		}


		public void StartTurn ()
		{
		round++;
				this.heroeDone = false;
				this.enemyDone = false;
				this.gameState.hero.actionPoints = this.gameState.initialHeroActionPoints;
				this.gameState.enemy.actionPoints = this.gameState.initialEnemyActionPoints;
				this.actionIndex = 0;
				this.enemyActionIndex = 0;
				Debug.Log ("started turn");
				if (this.heroActions.Count > 0) {
						this.heroActions [this.actionIndex].execute ();
						actionIndex += 1;
				}
		}

		
		public void HeroWon ()
		{
				Debug.Log ("Hero has won");
				gameState.hero.playerUI.playerWon ();
				this.heroActions = new List<TurnAction> ();
		}

		public void HeroLost ()
		{

		}

		public void EndTurn ()
		{
				if (!this.heroeDone) {
						this.heroeDone = true;
						this.StartTurn ();
//						if (this.enemyActions.Count > 0) {
//								this.enemyActions [this.enemyActionIndex].execute ();
//								this.enemyActionIndex += 1;
//								Debug.Log ("Enemy Action");
//						} else {
//								this.StartTurn ();
//
//						}
//				} else if (!this.enemyDone) {
//						enemyDone = true;
//						this.StartTurn ();
				} else {
						this.StartTurn ();
				}
				Debug.Log ("Turn Ended");
		}

		public void Done ()
		{
				Debug.Log ("Done");
				if (!heroeDone) {
						if (actionIndex < heroActions.Count) {
								Debug.Log ("we execute");
								bool success = heroActions [this.actionIndex].execute ();
								if (!success) {
										this.EndTurn ();
								}
								actionIndex += 1;
						} else {
								this.EndTurn ();
						}
				} else if (!enemyDone) {
						if (enemyActionIndex < enemyActions.Count) {
								enemyActions [this.enemyActionIndex].execute ();
								enemyActionIndex += 1;

								Debug.Log ("Enemy Action");
						} else {
								this.EndTurn ();
						}
				}

		}
	
		public void onOKPressed ()
		{
				Vector3 position = new Vector3 (0, 0, 0);
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.One.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = oneSprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
				ItemSomething item = icon.GetComponent<ItemSomething> ();
				if (condSlots [1].obj.tag.Equals ("LessThan")) {
						if (condSlots [0].obj.tag.Equals ("One")) {
								if (condSlots [2].obj.tag.Equals ("One")) {
										item.cond = new LessThan (condSlots [0].obj.GetComponent<ItemSomething> ().getInt, condSlots [2].obj.GetComponent<ItemSomething> ().getInt);	
								} else {
										item.cond = new LessThan (condSlots [0].obj.GetComponent<ItemSomething> ().getInt, gameState.hero.weapon.range);
								}	
						} else {
								if (condSlots [2].obj.tag.Equals ("One")) {
										item.cond = new LessThan (gameState.hero.weapon.range, condSlots [2].obj.GetComponent<ItemSomething> ().getInt);
								}
						}
				} else if (condSlots [1].obj.tag.Equals ("MoreThan")) {
						if (condSlots [0].obj.tag.Equals ("One")) {
								if (condSlots [2].obj.tag.Equals ("One")) {
										item.cond = new GreaterThan (condSlots [0].obj.GetComponent<ItemSomething> ().getInt, condSlots [2].obj.GetComponent<ItemSomething> ().getInt);	
								} else {
										item.cond = new GreaterThan (condSlots [0].obj.GetComponent<ItemSomething> ().getInt, gameState.hero.weapon.range);
								}	
						} else {
								if (condSlots [2].obj.tag.Equals ("One")) {
										item.cond = new GreaterThan (gameState.hero.weapon.range, condSlots [2].obj.GetComponent<ItemSomething> ().getInt);
								}
						}
				} else if (condSlots [1].obj.tag.Equals ("EqualTo")) {
						if (condSlots [0].obj.tag.Equals ("One")) {
								if (condSlots [2].obj.tag.Equals ("One")) {
										item.cond = new Equals (condSlots [0].obj.GetComponent<ItemSomething> ().getInt, condSlots [2].obj.GetComponent<ItemSomething> ().getInt);	
								} else {
										item.cond = new Equals (condSlots [0].obj.GetComponent<ItemSomething> ().getInt, gameState.hero.weapon.range);
								}	
						} else {
								if (condSlots [2].obj.tag.Equals ("One")) {
										item.cond = new Equals (gameState.hero.weapon.range, condSlots [2].obj.GetComponent<ItemSomething> ().getInt);
								}
						}
				} else if (condSlots [1].obj.tag.Equals ("Four")) {
						if (condSlots [0].obj.tag.Equals ("Hero")) {
								item.getInt = () => gameState.hero.GetDistance (gameState.enemy);
						} else {
								item.getInt = () => gameState.enemy.GetDistance (gameState.hero);
						}
				}
		}
	
		protected void addMoveIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.Move.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = moveSprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}

		public void addAttackIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.Attack.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = attachSprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;

		}
		
		public void addHeroIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.Hero.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = heroSprite;

				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}

		public void addMosterIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.Enemy.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = enemySprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}

		public void addRangeIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.Range.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = rangeSprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}
	
		public void addFourIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.Four.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = fourSprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}
	
		public void addThreeIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.Three.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = threeSprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}
	
		public void addTwoIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.Two.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = twoSprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}
	
		public void addOnenIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.One.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = oneSprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}
	
	
		public void addLessThanIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.LessThan.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = lessThanSprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}

	
		public void addMoreThanIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.MoreThan.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = moreThanSprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}
	
		public void addEqualToIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.EqualTo.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = equalToSprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}
		public void addBowIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.Bow.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = bowSprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}
	
		public void addPussyIcon (Vector3 position)
		{
				GameObject icon = (GameObject)Instantiate (iconPrefab);
				icon.tag = IconsEnum.Pussy.ToString ();
				Image image = icon.GetComponent<Image> ();
				image.sprite = pussySprite;
				icon.transform.position = position;
				icon.transform.parent = iconCanvas.transform;
		}


}
