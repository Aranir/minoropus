﻿using UnityEngine;
using System.Collections;

abstract public class TurnAction
{
		public GameState gameState;
		public Condition condition;
		abstract public bool execute ();
		void Start ()
		{
				this.condition = new TrueCondition ();
		}

}
