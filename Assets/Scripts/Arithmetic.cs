﻿using UnityEngine;
using System.Collections;
using System;

public class Plus
{
	public static Func<int> Build(Func<int> op1, Func<int> op2){
		return new Func<int>(() => (op1() + op2()));
	}
	
	public static Func<int> Build(int op1, Func<int> op2){
		return Build(() => op1, op2);
	}
	
	public static Func<int> Build(Func<int> op1, int op2)
	{
		return Build(op1, () => op2);
	}
	
	public static Func<int> Build(int op1, int op2)
	{
		return Build(() => op1, () => op2);
	}
}

public class Minus
{
	public static Func<int> Build(Func<int> op1, Func<int> op2){
		return new Func<int>(() => (op1() - op2()));
	}
	
	public static Func<int> Build(int op1, Func<int> op2){
		return Build(() => op1, op2);
	}
	
	public static Func<int> Build(Func<int> op1, int op2)
	{
		return Build(op1, () => op2);
	}
	
	public static Func<int> Build(int op1, int op2)
	{
		return Build(() => op1, () => op2);
	}
}

public class Times
{
	public static Func<int> Build(Func<int> op1, Func<int> op2){
		return new Func<int>(() => (op1() * op2()));
	}
	
	public static Func<int> Build(int op1, Func<int> op2){
		return Build(() => op1, op2);
	}
	
	public static Func<int> Build(Func<int> op1, int op2)
	{
		return Build(op1, () => op2);
	}
	
	public static Func<int> Build(int op1, int op2)
	{
		return Build(() => op1, () => op2);
	}
}

public class Divide
{
	public static Func<int> Build(Func<int> op1, Func<int> op2){
		return new Func<int>(() => (op1() / op2()));
	}
	
	public static Func<int> Build(int op1, Func<int> op2){
		return Build(() => op1, op2);
	}
	
	public static Func<int> Build(Func<int> op1, int op2)
	{
		return Build(op1, () => op2);
	}
	
	public static Func<int> Build(int op1, int op2)
	{
		return Build(() => op1, () => op2);
	}
}