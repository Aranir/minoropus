﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour,IDropHandler
{
		public GameObject obj = null;
		public GameObject condition = null;
		public TurnAction action;
		public GameObject gameEngineObject;
		public GameObject gameStateObject;
		public GameEngine gameEngine;
		public GameState gameState;
		public bool filled = false; 
		public bool rememberExtension = false;
		public int slotIndex;
		public ExpandCanvasScript expanderParent;
		
		

		public float height;

		public static bool isExpanded = false;

		// Use this for initialization
		void Start ()
		{
				//this.gameEngine = gameEngineObject.GetComponent<GameEngine> ();
				//this.gameState = gameStateObject.GetComponent<GameState> ();
		}

		public void Finished ()
		{

				Vector3 init = obj.GetComponent<ItemSomething> ().initialPos;
				obj.transform.position = init;
				if (expanderParent == null) {
						obj.transform.parent = GameObject.Find ("/SlotCanvas").transform;
				}
				if (obj.tag.Equals (IconsEnum.Attack.ToString ())) {
						gameEngine.addAttackIcon (init);
				}
		}
		public void OnDrop (PointerEventData data)
		{
				if (!filled) {
				
						filled = true;
						GameObject currObj = data.pointerDrag;
						IconsEnum tag = (IconsEnum)System.Enum.Parse (typeof(IconsEnum), currObj.tag, true);

						if (expanderParent != null) {
								obj = currObj;
						}
						Debug.Log ("TAG" + currObj.tag);


						if (tag == IconsEnum.Attack || tag == IconsEnum.Move) {
								bool dontExpand = (obj == currObj);

								obj = currObj;
								obj.transform.position = this.transform.position;

								ItemSomething comp = currObj.GetComponent<ItemSomething> ();
								comp.container = this;
								if (comp.shouldExpand () && !isExpanded && !dontExpand) {
										Debug.Log ("Expanding...");
										//GUI.DrawTexture(new Rect(10,10,60,60), new Texture(), ScaleMode.ScaleToFit, true, 10.0f);
										int left = 1;
										int right = 1;
										Image img = this.GetComponent<Image> ();

								
										GameObject canvas = GameObject.Find ("/SlotCanvas");
										Canvas canv = canvas.GetComponent<Canvas> ();
										CanvasGroup cgroup = canv.GetComponent<CanvasGroup> ();
										cgroup.interactable = false;
										cgroup.blocksRaycasts = false;

										GameObject canvas1 = GameObject.Find ("/ConditionCanvas");
										Canvas canv1 = canvas1.GetComponent<Canvas> ();
										CanvasGroup cgroup1 = canv1.GetComponent<CanvasGroup> ();
										cgroup1.interactable = false;
										cgroup1.blocksRaycasts = false;

										comp.isInteractible = false;

										GameObject expandcanvas = GameObject.Find ("/ExpandCanvas");
										ExpandCanvasScript expandCanvasScript = expandcanvas.GetComponent<ExpandCanvasScript> ();
										Canvas expcanv = expandcanvas.GetComponent<Canvas> ();
										CanvasGroup expgroup = expcanv.GetComponent<CanvasGroup> ();
										expgroup.blocksRaycasts = true;
										expgroup.alpha = 1.0f;


										ExpandCanvasScript.prevX = img.transform.position.x;
										ExpandCanvasScript.prevY = img.transform.position.y;
										Debug.Log (ExpandCanvasScript.midleX);
										img.transform.position = new Vector3 (ExpandCanvasScript.midleX, ExpandCanvasScript.midleY, 0);
										Debug.Log (img.transform.position);
										currObj.transform.position = img.transform.position;

										expandCanvasScript.addSlots (left, right, tag);
										expandCanvasScript.childSlot = this;

										CanvasGroup canvasGroup = canv.GetComponent<CanvasGroup> ();
										GameEngine.toDissappearCanvas = canvasGroup;
										isExpanded = true;
										rememberExtension = true;

								}

						} else if (currObj.tag.Equals ("LessThan") || currObj.tag.Equals ("MoreThan") || currObj.tag.Equals ("EqualTo") 
								|| currObj.tag.Equals ("Four") || currObj.tag.Equals ("Range") 
								|| currObj.tag.Equals ("One") || currObj.tag.Equals (IconsEnum.Hero.ToString ()) ||
								currObj.tag.Equals (IconsEnum.Enemy.ToString ())) {
								obj = currObj;
								currObj.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, 0);
						} else if (currObj.tag.Equals ("Condition")) {
								condition = currObj;
								currObj.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y + 60.0f, 0);
						
			
						} else {
								Debug.Log ("TAG:" + currObj.tag);
								Debug.Log ("Dropped the bass");
								Slot comp = currObj.GetComponent<Slot> ();
								obj = comp.obj;
								obj.transform.position = this.transform.position;
								if (comp != this)
										comp.obj = null;
						}
						if (expanderParent) {
								expanderParent.slotFilled (slotIndex, tag);
		
						}

					
				}
		}

		public void OnMouseDrag ()
		{
				Debug.Log ("Dropped the bass");

				if (obj != null) {
						Vector3 mousePos = Input.mousePosition;
						obj.transform.position = new Vector3 (mousePos.x, mousePos.y, 0);
				}
		}

		public void OnMouseDown ()
		{
				if (rememberExtension) {
						Debug.Log ("We are the heroes");
				}
		}

		// Update is called once per frame
		void Update ()
		{
	
		}
}
