﻿using UnityEngine;
using System.Collections;

public class Terrain : MonoBehaviour {

	public GameObject tile;
	public int sizeX = 32;
	public int sizeY = 32;

	// Use this for initialization
	void Start () {
		var gap  = tile.transform.renderer.bounds.size.x ;
		var nextX = 0;
		var nextY = 0;

		for (int i=0; i<sizeX; i++) {
			for(int j=0; j<sizeY; j++){
				GameObject terrain =  (GameObject) Instantiate(tile, new Vector3(nextX, 0, nextY), Quaternion.identity);
				terrain.transform.parent = this.transform;
				nextX += (int) gap;
			}
			nextX = 0;
			nextY += (int) gap;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
