﻿using UnityEngine;
using System.Collections;

public class Weapon {

	public int damage;
	public int activationCost;
	public int range;
	public Weapon(int damage, int activationCost, int range){
		this.damage = damage;
		this.activationCost = activationCost;
		this.range = range;
	}


}
