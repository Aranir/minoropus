﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ExpandCanvasScript : MonoBehaviour
{
		public GameObject slotPrefab;
		public GameEngine gameEngine;
		public static float midleX = 290.0f;
		public static float midleY = 100.0f;
		public GameState gameState;
		public static float prevX = 0;
		public static float prevY = 0;

		public int lefts;
		public int rights;
		public TurnAction action;
		public IconsEnum currentTag;
		public Slot childSlot;
		public GameObject icon1;
		public GameObject icon2;
	
		public List<GameObject> unityGameObjects = new List<GameObject> ();
		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{
			
		}

		public void createAction (IconsEnum icon)
		{
				if (icon == IconsEnum.Attack) {
						action = new AttackAction ();
				} else if (icon == IconsEnum.Move) {
						action = new MoveAction ();
				}
				action.condition = new TrueCondition ();
				action.gameState = gameState;

		}

		public void slotFilled (int index, IconsEnum tag)
		{
				Debug.Log ("Current tag " + currentTag.ToString ());
			
				if (currentTag.Equals (IconsEnum.Attack)) {
						if (index < 0) {
								AttackAction realAction = (AttackAction)action;
								realAction.attacker = gameState.hero;
						} else {
								AttackAction realAction = (AttackAction)action;
								realAction.target = gameState.enemy;
						}
				} else if (currentTag.Equals (IconsEnum.Move)) {
						MoveAction move = (MoveAction)action;
						if (index < 0) {
								move.entity = gameState.hero;
						} else {
								move.targetEntity = gameState.enemy;
						}
				}
				bool allfilled = true;
				foreach (GameObject go in unityGameObjects) {
						if (!go.GetComponent<Slot> ().filled) {
								allfilled = false;
						}
				}
				Debug.Log ("Is all filled?");
				if (allfilled) {
						Debug.Log ("Yes all filled" + action);
						childSlot.action = action;

						foreach (GameObject go in unityGameObjects) {
								Slot obj = go.GetComponent<Slot> ();
								obj.Finished ();
								Destroy (go);

						}
						unityGameObjects = new List<GameObject> ();

						childSlot.obj.transform.position = new Vector3 (prevX, prevY, 0);
						childSlot.transform.position = new Vector3 (prevX, prevY, 0);

						childSlot.expanderParent = null;
						GameEngine.toDissappearCanvas = null;

						GameObject expandcanvas = GameObject.Find ("/ExpandCanvas");
						ExpandCanvasScript expandCanvasScript = expandcanvas.GetComponent<ExpandCanvasScript> ();
						Canvas expcanv = expandcanvas.GetComponent<Canvas> ();
						CanvasGroup expgroup = expcanv.GetComponent<CanvasGroup> ();
						expgroup.blocksRaycasts = false;
						expgroup.interactable = false;
						expgroup.alpha = 0.0f;

						Slot.isExpanded = false;
						GameObject canvas = GameObject.Find ("/SlotCanvas");
						Canvas canv = canvas.GetComponent<Canvas> ();
						CanvasGroup cgroup = canv.GetComponent<CanvasGroup> ();
						cgroup.interactable = true;
						cgroup.blocksRaycasts = true;
						cgroup.alpha = 1.0f;

						//comp.isInteractible = false;
			

				}

		}
		

		public void addSlots (int l, int r, IconsEnum tag)
		{
				currentTag = tag;
				lefts = l;
				rights = r;
				float X = midleX;
				float Y = midleY;

				for (int i = 0; i <l; ++i) {

						Debug.Log ("Dropped the bass");
						X -= GameEngine.slotSize + GameEngine.space;
						GameObject slot = (GameObject)Instantiate (slotPrefab);
						Image img = slot.GetComponent<Image> ();
						img.color = Color.blue;
						Slot script = slot.GetComponent<Slot> ();
						script.slotIndex = -(i + 1);
						script.expanderParent = this;
//						unityGameObjects.Insert (0, slot);
						unityGameObjects.Add (slot);
						slot.transform.position = new Vector3 (X, Y, 0);
						slot.transform.parent = this.GetComponent<Canvas> ().transform;

				}

				X = midleX;
				Y = midleY;
	
				for (int i = 0; i <r; ++i) {
			
						Debug.Log ("Dropped the bass");
						X += GameEngine.slotSize + GameEngine.space;
						GameObject slot = (GameObject)Instantiate (slotPrefab);
						Image img = slot.GetComponent<Image> ();
						img.color = Color.blue;
						Slot script = slot.GetComponent<Slot> ();
						script.slotIndex = i;
						script.expanderParent = this;

						unityGameObjects.Add (slot);
						slot.transform.position = new Vector3 (X, Y, 0);
						slot.transform.parent = this.GetComponent<Canvas> ().transform;
				}
				gameEngine.showSlots = false;
				createAction (tag);


		}
}
