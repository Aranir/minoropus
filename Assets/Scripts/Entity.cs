﻿using UnityEngine;
using System.Collections;
using System;

public class Entity : MonoBehaviour
{

		public int health;
		public int actionPoints;
		public int moveCost;
		public Vector2 position;
		public Weapon weapon;
		public PlayerUI playerUI;

		public Entity (int health, int actionPoints, int moveCost, Vector2 position)
		{
				this.health = health;
				this.actionPoints = actionPoints;
				this.moveCost = moveCost;
				this.position = position;
		}

		public void equipWeapon (Weapon weapon)
		{
				this.weapon = weapon;
		}

		public bool moveToPosition (Vector2 newPostion)
		{
				if (this.actionPoints >= this.moveCost) {
						this.playerUI.move (this.position, newPostion);
						this.actionPoints -= moveCost;
						this.position = newPostion;
						
						return true;
				}
				return false;
		}

		public bool attack (Vector2 attackerPosition, Vector2 targetPosition)
		{
				if (this.weapon != null && this.weapon.activationCost <= this.actionPoints) {
						Debug.Log ("we play attack anaimation");
						this.actionPoints -= this.weapon.activationCost;
						if (weapon.range <= 1) {
								playerUI.attackShort (attackerPosition, targetPosition);

						} else {
								playerUI.attackRange (attackerPosition, targetPosition);
						}
						return true;
				}

				return false;
		}

		public void receiveDamage (int damage)
		{
				this.health -= damage;

		}

		public int GetDistance (Entity entity)
		{
				int deltaY = (int)(this.position.y - entity.position.y);
				int deltaX = (int)(this.position.x - entity.position.x);
				return Math.Abs (deltaX) + Math.Abs (deltaY);
		}

}
