﻿using UnityEngine;
using System.Collections;

public class PlayerUI : MonoBehaviour
{

		public GameEngine gameEngine;
		private float STEP = 10.0f;
		private float STEP_TOLERANCE = 5.0f;
		public float dampTime = 0.15f;
		private Vector3 velocityPos = Vector3.zero;
	
		private Vector3 target;
		private Vector3 direction;
		private Transform spritesAnim;
		public Transform victoryAnim;
		private bool stopped = true;
		private double victoryTimer;
	
		Vector3 oldPos;
		Vector3 newPos;
		Vector3 newDirection = new Vector3 (0, 0, 0);
	
		public int posX;
		public int posY;
	
		public float timer = -1;	
		public bool doneAcked = false;
	
		public string animation_file;
		public Transform arrow;
		public GameObject marrow;
		private Vector3 arrowTarget;
		private bool arrowLaunched = false;
	
		public enum playerState
		{
				idle, 
				moving
		}
	
		// Use this for initialization
		void Start ()
		{
				target = transform.position;
				oldPos = transform.position;
				newPos = transform.position;
				Renderer arrowRenderer = marrow.renderer;
				arrowRenderer.enabled = false;
		
				setPosition (new Vector2 (posX, posY));
				spritesAnim = transform.Find ("idle_anim");
				victoryAnim = transform.Find ("victory-anim");
//				victoryAnim.GetComponent<Animator> ().enabled = false;
//				victoryAnim.localScale = new Vector3 (0, 0, 0);
//				victoryAnim.GetComponent<Animator> ().Play ("victory-anim");
//				victoryAnim.renderer.enabled = false;

				
		}

		public void playerWon ()
		{
				victoryTimer = 2.3d;
//				victoryAnim.renderer.enabled = true;
				victoryAnim.GetComponent<Animator> ().enabled = true;

		}

		void FixedUpdate ()
		{
				
				if (victoryTimer < 0.0d) {
//						victoryAnim.renderer.enabled = false;
//						victoryAnim.GetComponent<Animator> ().enabled = false;

			
				} else {
						victoryTimer = victoryTimer - 0.01d;
//						victoryAnim.renderer.enabled = true;
//						victoryAnim.GetComponent<Animator> ().enabled = true;


				}
		}
	
		// Update is called once per frame
		void Update ()
		{	

			
		
				if (Input.GetKeyDown (KeyCode.UpArrow)) {
						//attackShort(new Vector2(0,0), new Vector2(0,1));
						attackRange (new Vector2 (10, 10), new Vector2 (10, 10));
				}
				if (Input.GetKeyDown (KeyCode.DownArrow)) {
						attackShort (new Vector2 (0, 0), new Vector2 (0, -1));
				}
				if (Input.GetKeyDown (KeyCode.RightArrow)) {
						attackShort (new Vector2 (0, 0), new Vector2 (1, 0));
				}
				if (Input.GetKeyDown (KeyCode.LeftArrow)) {
						attackShort (new Vector2 (0, 0), new Vector2 (-1, 0));
				}
		
		
				transform.rotation = Quaternion.Lerp (transform.rotation, Quaternion.Euler (direction), 0.1f);
		
				if (!stopped) {
						transform.position = Vector3.SmoothDamp (transform.position, target, ref velocityPos, dampTime);
			
						if (Vector3.Distance (target, transform.position) < STEP_TOLERANCE) {
								transform.position = target;
								oldPos = transform.position;
								newPos = transform.position;
								direction = Vector3.back;
								stopped = true;
								// done
								gameEngine.Done ();
						}
				}
		
				// Rotation animation end
				if (timer > 0.0f) {
						timer -= Time.deltaTime;
				} else {
						if (doneAcked) {
								doneAcked = false;
								Debug.Log ("done melee");
								gameEngine.Done ();
						}
				}
		
				if (arrowLaunched) {
						Vector3 _direction = (arrowTarget - transform.position).normalized;
						Quaternion _lookRotation = Quaternion.LookRotation (_direction);
						marrow.transform.rotation = _lookRotation;
						marrow.transform.Rotate (Vector3.up, -90);
						marrow.transform.Rotate (Vector3.left, -90);
			
						marrow.transform.position = Vector3.SmoothDamp (marrow.transform.position, arrowTarget, ref velocityPos, 0.3f);
						if ((marrow.transform.position - arrowTarget).magnitude < 1.0f) {
								marrow.transform.position = new Vector3 (0, -20.0f, 0);
								arrowLaunched = false;
								Debug.Log ("done arrow");
								marrow.renderer.enabled = false;
								gameEngine.Done ();
						}
				}
		}
	
		public void setPosition (Vector2 pos)
		{
				Vector3 hardPos = new Vector3 (pos.x * STEP, transform.position.y, pos.y * STEP);
		
				transform.position = hardPos;
				target = transform.position;
				oldPos = transform.position;
				newPos = transform.position;
				direction = Vector3.back;
				stopped = true;
		}
	
		public bool attackShort (Vector2 playerPos, Vector2 ennemyPos)
		{
		
//				Vector2 diff = playerPos - ennemyPos;
//				Vector2 ndiff = diff.normalized;
		
//				if (ndiff == new Vector2 (1, 0)) {
//						transform.rotation = Quaternion.Euler (new Vector3 (0, 180, 0));
//				} else if (ndiff == new Vector2 (-1, 0)) {
//						transform.rotation = Quaternion.Euler (new Vector3 (0, 180, 0));
//				} else if (ndiff == new Vector2 (0, 1)) {
//						transform.rotation = Quaternion.Euler (new Vector3 (0, 180, 0));
//				} else if (ndiff == new Vector2 (0, -1)) {
//						transform.rotation = Quaternion.Euler (new Vector3 (0, 180, 0));
//				} else {
//						return false;
//				}
		
				timer = 1;
				doneAcked = true;
		
				spritesAnim.GetComponent<Animator> ().Play ("attack-anim");
				return true;
		}
	
		public void attackRange (Vector2 playerPos, Vector2 ennemyPos)
		{
				marrow.renderer.enabled = true;
				arrowTarget = new Vector3 (ennemyPos.x * STEP, 9.0f, ennemyPos.y * STEP);
				marrow.transform.position = transform.position;
				arrowLaunched = true;
		}
	
		public bool move (Vector2 oldPosition, Vector2 newPosition)
		{
				if (!stopped) {
						return false;
				}
		
				stopped = false;
		
				Vector2 diff = newPosition - oldPosition;
				Vector2 ndiff = diff.normalized;
		
				if (ndiff == new Vector2 (1, 0)) {
						newPos.x = oldPos.x + STEP;
						newDirection = new Vector3 (0, 270, 0);
				} else if (ndiff == new Vector2 (-1, 0)) {
						newPos.x = oldPos.x - STEP;
						newDirection = new Vector3 (0, 90, 0);
				} else if (ndiff == new Vector2 (0, 1)) {
						newPos.z = oldPos.z + STEP;
						newDirection = new Vector3 (0, 0, 0);
				} else if (ndiff == new Vector2 (0, -1)) {
						newPos.z = oldPos.z - STEP;
						newDirection = new Vector3 (0, 180, 0);
				} else {
						return false;
				}
		
				target = newPos;
				direction = newDirection;
		
				return true;
		
		}
	
}
