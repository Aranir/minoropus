﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class GameState : MonoBehaviour
{

		public Entity hero;
		public Entity enemy;
		public int xSize = 32;
		public int ySize = 32;
		public GameEngine gameEngine;
		public int initialHeroActionPoints;
		public int initialEnemyActionPoints;
		public bool movingIsEnough;



		// Use this for initialization
		void Start ()
		{
//				this.hero = new Entity (10, 10, 4, new Vector2 (this.xSize / 2, 0));
//				this.enemy = new Entity (10, 10, 4, new Vector2 (this.xSize / 2, ySize - 3));
				Weapon heroWeapon = new Weapon (4, 3, 1);
				this.hero.equipWeapon (heroWeapon);
				Debug.Log ("Game state initialized" + this.hero.health);
				this.initialHeroActionPoints = hero.actionPoints;
				this.initialEnemyActionPoints = enemy.actionPoints;
				movingIsEnough = false;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public bool Move (Entity entity, Vector2 newPosition)
		{
				Debug.Log ("we move");
				bool success = entity.moveToPosition (newPosition);
				if (movingIsEnough && distance (hero.position, enemy.position) <= 1) {
						gameEngine.HeroWon ();
				}
				return success;

		}

		public int distance (Vector2 point1, Vector2 point2)
		{
				return (int)(Math.Abs (point1.x - point2.x) + Math.Abs (point1.y - point2.y));
		}

		public bool insideGameBoard (Vector2 position)
		{
				return position.x < xSize && position.x >= 0 && position.y < ySize && position.y >= 0;

		}
		public bool Attack (Entity attacker, Entity target)
		{
				Debug.Log ("We try to attacking target health " + target.health);
				bool success = attacker.attack (attacker.position, target.position);
				if (success && distance (attacker.position, target.position) <= attacker.weapon.range) {
						Debug.Log ("We are attacking");
						target.receiveDamage (attacker.weapon.damage);
						if (target.health <= 0) {
								if (hero == attacker) {
										gameEngine.HeroWon ();
								} else {
										gameEngine.HeroLost ();
								}
						}
				}
				return success;
		}
}
