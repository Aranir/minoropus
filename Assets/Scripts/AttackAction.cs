﻿using UnityEngine;
using System.Collections;

public class AttackAction : TurnAction
{


		public Entity attacker;
		public Entity target;
	

		// Use this for initialization
		void Start ()
		{
				this.condition = new TrueCondition ();
		}

		
		override public bool execute ()
		{
				Debug.Log ("higher attack");
				if (base.condition.test ()) {
						return this.gameState.Attack (attacker, target);
				} else {
						return true;	
				}
		}
}
