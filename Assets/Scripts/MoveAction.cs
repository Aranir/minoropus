﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MoveAction : TurnAction
{
		public Entity entity;
		public Entity targetEntity;

		void Start ()
		{
				this.condition = new TrueCondition ();
		}

		Vector2 getNewPosition ()
		{
				int xDelta = (int)(entity.position.x - targetEntity.position.x);
				int yDelta = (int)(entity.position.y - targetEntity.position.y);
				Vector2 newPosition = entity.position;
		
				int next;
				if (Math.Abs (xDelta) > 0 && Math.Abs (yDelta) > 0) {
						System.Random random = new System.Random ();
						next = random.Next (0, 1);
				} else if (Math.Abs (xDelta) > 0) {
						next = 0;
				} else {
						next = 1;
				}
				if (next == 0) {
						if (xDelta < 0) {
								newPosition = new Vector2 (entity.position.x + 1, entity.position.y);
						} else {
								newPosition = new Vector2 (entity.position.x - 1, entity.position.y);
						}
				} else {
						if (yDelta < 0) {
								newPosition = new Vector2 (entity.position.x, entity.position.y + 1);
						} else {
								newPosition = new Vector2 (entity.position.x, entity.position.y - 1);
				
						}
			
				}
				return newPosition;
		}

		override public bool execute ()
		{
				Debug.Log ("Moving");
				if (base.condition.test ()) {
						Vector2 newPosition = this.getNewPosition ();
						if (Math.Abs (entity.position.x - targetEntity.position.x) > 1 || Math.Abs (entity.position.y - targetEntity.position.y) > 1) {
								Debug.Log (newPosition);
								return gameState.Move (this.entity, newPosition);
						
						} else {
								return gameState.Move (this.entity, this.entity.position);
						}
				} else {
						return true;	
				}
		}

}
