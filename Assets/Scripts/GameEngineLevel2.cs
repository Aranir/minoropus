﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;



public class GameEngineLevel2 : GameEngine
{


		// Use this for initialization
		void Start ()
		{
				//				this.slotCanvas.gameObject.SetActive (false);
				base.Start ();
				createIcons ();
		}

	// Update is called once per frame
	void Update ()
	{
		base.Update ();
	}
	
		void createIcons ()
		{
				int currentY = 490;
		
				int delta = 35;
				int x3 = 990;
				int x2 = x3 - delta;
				int x1 = x2 - delta;
		
		
				addHeroIcon (new Vector3 (x1, currentY, 0));
				addMosterIcon (new Vector3 (x3, currentY, 0));
		
				currentY = currentY - delta - slotSize;
		
				addMoveIcon (new Vector3 (x1, currentY, 0));
				addAttackIcon (new Vector3 (x2, currentY, 0));
		}

}
