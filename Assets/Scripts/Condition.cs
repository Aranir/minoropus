﻿using UnityEngine;
using System.Collections;

using System;

public abstract class Condition {
	abstract public bool test();
}

public class TrueCondition:Condition{
	override public bool test(){
		return true;
	}
}

public class FalseCondition:Condition{
	override public bool test(){
		return false;
	}
}

public class And:Condition{
	private Condition cond1;
	private Condition cond2;
	public And(Condition cond1, Condition cond2){
		this.cond1 = cond1;
		this.cond2 = cond2;
	}
	override public bool test(){
		return cond1.test () && cond2.test ();
	}
}

public class Or:Condition{
	private Condition cond1;
	private Condition cond2;
	public Or(Condition cond1, Condition cond2){
		this.cond1 = cond1;
		this.cond2 = cond2;
	}
	override public bool test(){
		return cond1.test () || cond2.test ();
	}
}

public class Not:Condition{
	private Condition condition;
	public Not(Condition condition){
		this.condition = condition;
	}
	override public bool test(){
		return !this.condition.test ();
	}
}

public class GreaterThan:Condition{
	private Func<int> op1;
	private Func<int> op2;
	public GreaterThan(Func<int> op1, Func<int> op2){
		this.op1 = op1;
		this.op2 = op2;
	}

	public GreaterThan(int cst,Func<int> op2) : this(() => cst, op2){
	}

	public GreaterThan(Func<int> op1, int cst) : this(op1, () => cst){
	}

	override public bool test() {
		return op1() > op2();
	}
}

public class LessThan:Condition{
	private Func<int> op1;
	private Func<int> op2;
	public LessThan(Func<int> op1, Func<int> op2){
		this.op1 = op1;
		this.op2 = op2;
	}
	
	public LessThan(int cst,Func<int> op2) : this(() => cst, op2){
	}
	
	public LessThan(Func<int> op1, int cst) : this(op1, () => cst){
	}
	
	override public bool test() {
		return op1() < op2();
	}
}

public class Equals:Condition{
	private Func<int> op1;
	private Func<int> op2;
	public Equals(Func<int> op1, Func<int> op2){
		this.op1 = op1;
		this.op2 = op2;
	}
	
	public Equals(int cst,Func<int> op2) : this(() => cst, op2){
	}
	
	public Equals(Func<int> op1, int cst) : this(op1, () => cst){
	}
	
	override public bool test() {
		return op1() == op2();
	}
}
