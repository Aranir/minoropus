﻿using UnityEngine;
using System.Collections;

public class GameState1 : GameState
{

		// Use this for initialization
		void Start ()
		{
				Weapon heroWeapon = new Weapon (4, 3, 1);
				this.hero.equipWeapon (heroWeapon);
				this.initialHeroActionPoints = hero.actionPoints;
				this.initialEnemyActionPoints = enemy.actionPoints;
				movingIsEnough = true;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		new public bool Move (Entity entity, Vector2 newPosition)
		{
				Debug.Log ("we move");
				bool success = entity.moveToPosition (newPosition);
				if (movingIsEnough && distance (hero.position, enemy.position) <= 1) {
						gameEngine.HeroWon ();
				}
				return success;
		
		}
}
