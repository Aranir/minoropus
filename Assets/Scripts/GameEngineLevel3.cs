﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameEngineLevel3 : GameEngine
{

		// Use this for initialization
		void Start ()
		{

//				this.showSlots = true;
//
////				this.slotCanvas.gameObject.SetActive (false);
//				this.actionIndex = 0;
//				this.enemyActionIndex = 0;
//				this.heroActions = new List<TurnAction> ();
//				this.enemyActions = new List<TurnAction> ();
//				int numberOfSlots = 10;
//				for (int i = 0; i < numberOfSlots; i++) {
//						GameObject slot = (GameObject)Instantiate (slotPrefab);
//						Slot script = slot.GetComponent<Slot> ();
//						script.gameEngine = this;
//						script.gameState = this.gameState;
//						slot.transform.position = new Vector3 (GameEngine.slotSize + (GameEngine.slotSize + space) * i, GameEngine.slotSize, 0);
//						slot.transform.parent = slotCanvas.transform;
//				}
//		
//				for (int i = 0; i < numberOfSlots; i++) {
//						GameObject slot = (GameObject)Instantiate (slotPrefab);
//						Slot script = slot.GetComponent<Slot> ();
//						script.gameEngine = this;
//						script.gameState = this.gameState;
//						slot.transform.position = new Vector3 (GameEngine.slotSize + (GameEngine.slotSize + space) * i, GameEngine.slotSize * 2 + 20, 0);
//						slot.transform.parent = slotCanvas.transform;
//				}
//		
//				for (int i = 0; i < numberOfSlots; i++) {
//						GameObject slot = (GameObject)Instantiate (slotPrefab);
//						Slot script = slot.GetComponent<Slot> ();
//						script.gameEngine = this;
//						script.gameState = this.gameState;
//						slot.transform.position = new Vector3 (GameEngine.slotSize + (GameEngine.slotSize + space) * i, GameEngine.slotSize * 3 + 40, 0);
//						slot.transform.parent = slotCanvas.transform;
//				}
				base.Start ();
				addIcons ();

		}


		void addIcons ()
		{

				int currentY = 490;

				int delta = 35;
				int x3 = 990;
				int x2 = x3 - delta;
				int x1 = x2 - delta;


				addHeroIcon (new Vector3 (x1, currentY, 0));
				addMosterIcon (new Vector3 (x3, currentY, 0));

				currentY = currentY - delta - slotSize;

				addMoveIcon (new Vector3 (x1, currentY, 0));
				addAttackIcon (new Vector3 (x2, currentY, 0));
				addPussyIcon (new Vector3 (x3, currentY, 0));

				currentY = currentY - delta - slotSize;

				addRangeIcon (new Vector3 (x1, currentY, 0));
				currentY = currentY - delta - slotSize;

				currentY = currentY - delta - slotSize;

				addEqualToIcon (new Vector3 (x1, currentY, 0));
				addLessThanIcon (new Vector3 (x2, currentY, 0));
				addMoreThanIcon (new Vector3 (x3, currentY, 0));

				currentY = currentY - delta - slotSize;
				addOnenIcon (new Vector3 (x1, currentY, 0));
				addTwoIcon (new Vector3 (x2, currentY, 0));
				addThreeIcon (new Vector3 (x3, currentY, 0));
				currentY = currentY - delta;

				addFourIcon (new Vector3 (x1, currentY, 0));

		          
		}
		void createConditionTab ()
		{

		}
	
		// Update is called once per frame
		void Update ()
		{
				base.Update ();
		}
}
