﻿using UnityEngine;
using System.Collections;
using System;

public class ItemSomething : MonoBehaviour
{
		public bool isInteractible = true;
		public Slot container;
		public Func<int> getInt = null;
		public Condition cond = null;

		public Vector3 initialPos;


		// Use this for initialization
		void Start ()
		{
		Debug.Log ("Current init pos " + this.transform.position);

			initialPos = this.transform.position;
		}

		public bool shouldExpand ()
		{
				return true;
		}

		public void OnMouseDrag ()
		{
				Debug.Log ("Drag");
				if (!isInteractible) {
						return;
				}
				Vector3 mousePos = Input.mousePosition;
				transform.position = mousePos;
		}

		public void OnMouseRelease ()
		{
				Debug.Log ("On release");
				Vector3 mousePos = Input.mousePosition;
				transform.position = new Vector3 (mousePos.x, mousePos.y, 0);
				this.transform.position = new Vector3 (1000, 10, 0);
				container.action = null;
				container.filled = false;
				container = null;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public void OnMouseClick ()
		{
				Debug.Log ("Awesome");
		}
}
