﻿using UnityEngine;
using System.Collections;

public class ArrowScript : MonoBehaviour {
	
	private Vector3 velocityPos = Vector3.zero;
	public GameObject target;
	public GameObject player;
	private GameObject marrow;
	public GameObject arrow;
	

	// Use this for initialization
	void Start () {
		marrow = (GameObject) Instantiate(arrow, player.transform.position, Quaternion.identity);
		
		GameObject go = GameObject.Find("Main Camera");
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 _direction = (target.transform.position - player.transform.position).normalized;
		Quaternion _lookRotation = Quaternion.LookRotation(_direction);
		marrow.transform.rotation = _lookRotation;
		marrow.transform.Rotate (Vector3.up, -90);
		marrow.transform.Rotate (Vector3.left, -90);
		
		marrow.transform.position = Vector3.SmoothDamp(marrow.transform.position, target.transform.position, ref velocityPos, 0.15f);
		if (marrow.transform.position == target.transform.position) {
			marrow.transform.position = player.transform.position;
		}
	}
}
